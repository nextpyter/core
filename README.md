# nextpyter core
a mock environment for NextPyter

# how do I do everything?
The first thing you need to do is to write down your machine's Docker group ID.
<br>
You can find it by using the `id` command:
```bash
id
uid=1000(macca) gid=1000(macca) groups=1000(macca),10(wheel),976(docker)
```
In this case, 976 is the ID we are looking for.
<br>
This step is essential if you want your `core` instance to be able to share files with other Docker containers (i.e. Nextcloud): you need to run the `app` container in the `compose.yml` file with the right user and group, otherwise you'll get permission errors.
<br>
Every other container that access the `nextpyter_binds` volume **has to be mapped to the same user and group ids** you are using here!
<br>
You can map the ids like this:
```yaml
app:
    # more stuff ...
    user: 1000:976
    # other stuff... 
```
You can use the same mapping in every other container you want to link to `core`
- create a new docker volume for nextcloud called `nextpyter_binds` (or whatever name you want) and attach it to the `daemon` container
    * `docker volume create nextpyter_binds`
    * `docker volume inspect nextpyter_binds`
    * copy the "Mountpoint" field and paste it as the value of the `BIND_VOLUME_PATH` variable in the `app` container environment (inside the `compose.yml` file)
- run `docker compose up -d`
- launch migrations
    * `cd rqlite/migrations`
    * `cp .env.example .env`
    * `./migrate.sh`
- create the redis stream
    * `docker compose exec queue redis-cli`
    * run `XGROUP CREATE streams:images_data builders $ MKSTREAM`
    * exit

# how do I read the documentation
- access the `swagger` service at `http://localhost:8081/docs`
- paste the following URL inside the bar: `http://localhost:8081/daemon/json-schema`