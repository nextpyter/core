const required_roles = [
    "nextpyter-cfg-routes",
    "nextpyter-notebooks-routes",
];

const introspectAccessToken = async (res) => {

    const access_token = res.variables.cookie_token.trim().length > 0 
        ? res.variables.cookie_token
        : res.variables.header_token;
    const auth_creds = `${res.variables.oauth_client_id}:${res.variables.oauth_client_secret}`
    let basic_auth_header = `Basic ${btoa(auth_creds)}`;

    /**
     * since auth_request only allows 2xx, 401 and 403 error codes we must 
     * tap into our creative side and modify the "no_auth_reason" variable
     * that we will filter in the corresponding "catch block" (the error_page directive
     * in the proxy configuration file)
     */

    if(access_token.trim().length === 0){
        res.variables.no_auth_reason = 'empty_token';
        res.return(401);
        return;
    }

    const introspection_response = await res.subrequest(
        `.perform_introspection`,
        `token=${access_token}&auth=${basic_auth_header}`,
    );

    if (introspection_response.status != 200) {
        res.variables.no_auth_reason = `bad_introspection`;
        res.return(401);
        return;
    }
    processToken(res, introspection_response);

}

const processToken = (res, introspection_reply) => {
    try {
        const parsed_json = JSON.parse(introspection_reply.responseText);
        const stuff = evaluateToken(parsed_json, required_roles);
        res.error(introspection_reply.responseText);

        if (!stuff.active) {
            res.variables.no_auth_reason = 'not_active';
            res.return(401);
            return;
        }
        if (!stuff.authorized) {
            res.variables.no_auth_reason = 'no_roles';
            res.return(401);
            return;
        }

        res.status = 204;
        res.sendHeader();
        res.finish();

    } catch (err) {
        res.error(err);
        // using 400 to express a failure while parsing json is better than 401
        res.variables.no_auth_reason = 'cannot_parse';
        res.return(401);
    }
}

const evaluateToken = (json, roles) => {

    if (json.active == true) {

        // a client is authorized only if it satisfies all the required roles
        const authorized = json.realm_access.roles
            .filter(role => roles.includes(role)).length === roles.length;

        return {
            active: true,
            authorized,
        }
    } else {
        // should return 401 Unauthorized!
        return {
            active: false,
            authorized: false,
        }
    }

}

export default {
    introspectAccessToken
}
