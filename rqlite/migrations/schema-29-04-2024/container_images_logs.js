// this table represents the changes that the container images undergo
// while being pulled
// every row represents a state change, with an additional (optional) comment

// R: requested
// E: errored
// P: pulling
// D: done (pulled)

// there are no enums in sqlite so the implementation is bizarre

exports.up = async (conn) => {
    console.log("up with container_images_logs");
    let res = await conn.execute([
        `
            create table if not exists container_images_logs (
                id varchar(32) not null,
                status char not null default 'R' check(status in ('R', 'E', 'P', 'D')),
                timestamp_utc timestamp not null default current_timestamp,
                comment text,
                primary key(id, timestamp_utc),
                foreign key (id) references container_images(id) on delete cascade
            );
        `,
        `
            create index if not exists sort_id on container_images_logs(id);
        `, // because the primary key is composite
        `
            create index if not exists sort_status on container_images_logs(status);
        `,
    ])
    if (res.hasError()) {
        const error = res.getFirstError()
        console.error(error)
    }
}

exports.down = async (conn) => {
    console.log("down with container_images_logs");
    let res = await conn.execute([
        `PRAGMA foreign_keys = OFF;`,
        `drop table if exists container_images_logs`,
        `drop index if exists sort_id`,
        `drop index if exists sort_status`,
        `PRAGMA foreign_keys = OFF;`,
    ]);
    if (res.hasError()) {
        const error = res.getFirstError()
        console.error(error)
    }
}