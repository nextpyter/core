// this table represents the available container images that the 
// daemon can spawn.
// no other image is allowed

exports.up = async (conn) => {
    console.log("up with container_images");
    
    let res = await conn.execute([
        `
            create table if not exists container_images (
                id varchar(32) not null unique,
                name varchar(255) not null,
                tag varchar(255) not null
            )
        `,
        `create unique index if not exists uq_name_tag on container_images(name, tag)`
    ])
    if (res.hasError()) {
        const error = res.getFirstError()
        console.error(error)
    }
}

exports.down = async (conn) => {
    console.log("down with container_images");

    let res = await conn.execute([
        `PRAGMA foreign_keys = OFF;`,
        `drop table if exists container_images`,
        `drop index if exists uq_name_tag`,
        `PRAGMA foreign_keys = ON;`
    ]);
    if (res.hasError()) {
        const error = res.getFirstError()
        console.error(error)
    }
}